angular.module('myApp', ['oi.file']);

function PhotosCtrl($scope) {
    $scope.photos = {};
    $scope.photos.blue = bluePhotos;
    $scope.photos.yellow = yellowPhotos;

    $scope.blueOptions = {
        change: function(file) {
            console.log(file);
            file.$upload('uploadBlue.rb', $scope.photos.blue, {allowedType: ["jpeg", "jpg", "png"]}).then(
                function (data) {
                    console.log('upload success', data)
                },
                function (data) {
                    //console.log('upload error', data);
                    $scope.errors = angular.isArray($scope.errors) ? $scope.errors.concat(data.response) : [].concat(data.response);
                    $scope.del($scope.getIndexById(data.item.id));
                },
                function (data) {
                    console.log('upload notify', data)
                });
        }
    }

    $scope.yellowOptions = {
        change: function(file) {
            console.log(file);
            file.$upload('uploadYellow.rb', $scope.photos.blue, {allowedType: ["jpeg", "jpg", "png"]}).then(
                function (data) {
                    console.log('upload success', data)
                },
                function (data) {
                    console.log('upload error', data);
                    $scope.errors = angular.isArray($scope.errors) ? $scope.errors.concat(data.response) : [].concat(data.response);
                    console.log(data.item.id);
                    $scope.del($scope.getIndexById(data.item.id));
                },
                function (data) {
                    console.log('upload notify', data)
                });
        }
    }

}
