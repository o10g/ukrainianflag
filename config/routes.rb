Wearukraine::Application.routes.draw do
  devise_for :admins
  get "about/index"
  root to: 'photos#index'
  resources :photos, only: [:create, :index, :show]

  namespace :admin do
    root to: 'photos#index'
    resources :photos, only: [:destroy]
  end
end
