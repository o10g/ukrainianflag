class PhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def store_dir
    "system/uploads/photos/#{model.id}"
  end

  version :thumb do
    process resize_to_fill: [80,80]
  end
  
  version :preview do
    process resize_to_fit: [1000,1000]
  end
end
