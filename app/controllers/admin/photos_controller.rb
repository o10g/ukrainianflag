class Admin::PhotosController < AdminController
  def index
    @photos = Photo.all
  end

  def destroy
    Photo.destroy(params[:id])
    redirect_to :admin_root, notice: 'Photo deleted'
  end
end
