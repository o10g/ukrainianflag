class PhotosController < ApplicationController
  respond_to :html, :js

  def index
    @blue_photos      = Photo.blue
    @yellow_photos    = Photo.yellow
    @new_blue_photo   = Photo.new
    @new_yellow_photo = Photo.new
  end

  def show
    @blue_photos      = Photo.blue
    @yellow_photos    = Photo.yellow
    @new_blue_photo   = Photo.new
    @new_yellow_photo = Photo.new

    @photo = Photo.find(params[:id])
    render :index
  end

  def create
    @photo = Photo.create!(params.require(:photo).permit(:file, :color))

    respond_to do |format|
      format.html { redirect_to :root }
      format.js
    end
  end
end
