class Photo < ActiveRecord::Base
  mount_uploader :file, PhotoUploader

  scope :blue, ->{ where(color: 'blue') }
  scope :yellow, ->{ where(color: 'yellow') }

  validates :color, inclusion: {in: %w[blue yellow]}, presence: true
  validates :file, presence: true
end
