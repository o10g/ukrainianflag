module ApplicationHelper

def image_url(file)
  request.protocol + request.host_with_port + path_to_image(file)
end

end
