// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


var DropManager = function(target){
  var $target = $(target);

  var methods = {
    dragEnter: function (event) {
      event.stopPropagation();
      event.preventDefault();
    },

    dragOver: function (event) {
      event.stopPropagation();
      event.preventDefault();
      $target.addClass("highlighted");
    },

    dragLeave: function (event) {
      event.stopPropagation();
      event.preventDefault();
      $target.removeClass("highlighted");
    },

    drop: function (event) {
      event.stopPropagation();
      event.preventDefault();
      event.originalEvent.dataTransfer.dropEffect = 'copy';

      methods.traverseFiles(event);
      $target.removeClass("highlighted");
    },

    /**
     * Clicking on link will dispatch "Open file" dialog
     *
     * @param event
     */
    chooseFiles: function (event) {
      event.preventDefault();
      $target.find("input[type=file]").click();
    },

    /**
     * Handle file drag&drop or file select
     * Pass file to upload function
     *
     * @param event File drop or Input select event
     */
    traverseFiles: function (event) {
      var file = event.dataTransfer ? event.dataTransfer.files[0] : ((event.originalEvent && event.originalEvent.dataTransfer) ? event.originalEvent.dataTransfer.files[0] : event.currentTarget.files[0]);

      if (typeof file !== "undefined" && file.type.match('image.*')) {
        // show information about asset
        $target.file = file;
        methods.previewIcon(file);
      }
      else {
        alert("No image selected!");
      }
    },

    /**
     * Read uploaded file and prepare to sending
     *
     * @param callback Callback after file is read
     */
    previewIcon: function (file) {
      var reader = new FileReader();

      reader.onload = function (event) {
        $target.find(".face-preview").attr("src", event.target.result).removeClass("hidden");
      };
      reader.readAsDataURL(file);
    },

    /**
     * Save file
     * @returns {boolean}
     */
    uploadFile: function(event){
      if (!$target.file || $(event.target).attr("disabled")) return false;

      var $form = $target.find("form"),
          $fileInput = $target.find(".file-input"),
          $colorInput = $target.find(".color-input"),
          $uploadButton = $(event.target),
          formData = new FormData(),
          url = $form.attr("action"),
          token = $('meta[name="csrf-token"]').attr('content');

      formData.append($fileInput.attr("name"), $target.file);
      formData.append($colorInput.attr("name"), $colorInput.val());
      //formData.append("authenticity_token", $('meta[name=csrf-token]').attr("content"));

      $uploadButton.addClass("in-progress").attr("disabled", true);

      $.ajax({
        url: url,
        dataType: "script",
        processData: false,
        contentType: false,
        type: "POST",
        data: formData,
        headers: {
          'X-CSRF-Token': token
        },
        success: function(response){
          //$target.find(".face-preview").attr("src", "").addClass("hidden");
          //$form.get(0).clear();
        },
        complete: function(){
          $target.find(".face-preview").attr("src", "").addClass("hidden");
          $target.file = null;
          $uploadButton.removeClass("in-progress").attr("disabled", false);
        }
      });
    }
  };

  $target
      .bind("dragenter", methods.dragEnter)
      .bind("dragover", methods.dragOver)
      .bind("dragleave", methods.dragLeave)
      .bind("drop", methods.drop);

  $target.find(".face-add-button").bind("click", methods.chooseFiles).end()
         .find("input[type=file]").bind("change", methods.traverseFiles).end()
         .find(".face-upload-button").bind("click", methods.uploadFile);
};

var Tools = {
  calculateFaceSide: function(amount, width, height, max){
    var side = Math.floor(Math.sqrt(width * height / amount)),
        wAmount = Math.floor(width / side),
        hAmount = Math.floor(height / side);

    while( wAmount * hAmount < amount){
      side= side - 1;
      wAmount = Math.floor(width / side);
      hAmount = Math.floor(height / side);
    }

    return Math.min(side, max);
  },

  // show modal popup with face preview
  showFace: function(face){
    var faceUrl = $("img", face).attr("data-orig-src"),
        shareUrl = $("img", face).attr("data-share-url");

    var $modal = $("#facePreview"),
        $fbLike = $("<div class='fb-like' data-href='" +  shareUrl + "' data-layout='button_count' data-action='like' data-show-faces='true' data-share='true'></div>");
    $modal.find(".modal-body img").attr("src", faceUrl);
    $modal.find(".social-links").html("").append($fbLike);
    FB.XFBML.parse($fbLike.get(0));

    $modal.modal('show');
  },

  //minimize faces to fit flag
  drawFaces : function(){
    $("#flag .flag-faces").each(function(){
      var $flagPart = $(this),
          faces = $flagPart.find("li"),
          faceSide = Tools.calculateFaceSide(faces.length, $flagPart.width(), $flagPart.height(), 80);

      faces.each(function(){
        $(this).get(0).style.width = faceSide + "px";
        $(this).get(0).style.height = faceSide + "px";
      });
    })
  }
};



jQuery( document ).ready(function( $ ) {

  var $flag = $("#flag"),
      $newFace = $(".flag-face--new"),
      $newFaceForm = $(".flag-face-form"),
      $activePhoto = $flag.find(".flag-faces [data-active]");

  var bluePartDropManager   = new DropManager(".flag-part--blue .flag-face-form"),
      yellowPartDropManager = new DropManager(".flag-part--yellow .flag-face-form");

  $flag.find(".flag-faces").on("click", "li", function(event){
    Tools.showFace(event.target);
  });

  Tools.drawFaces();
  $(window).bind("resize", Tools.drawFaces);

  if ($activePhoto[0]){
    Tools.showFace($activePhoto);
  } else{
    // show About on start
    $("#about").modal('show');
  }
});
